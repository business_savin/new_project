# README #

Данный репозиторий используется для создания нового проекта верстки со всеми необходимыми файлами для установки
ниже предоставлен список конфигов, которые будут заполняться по мене необходимости. И модифицироваться.
При установке просто удалять не нужные строки, ибо в json нельзя использвоать комментарии, так бы в файл записал все

### NPM ###
{
  "name": "New project",
  "version": "1.0.0",
  "main": "index.html",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@bitbucket.org:business_savin/* * *.git"
  },
  "author": "Sava (Savin Anton)",
  "license": "ISC",
  "homepage": "http://cobbi.ru/",
  "devDependencies": {
    "browser-sync": "^2.14.0",
    "gulp": "^3.9.1",
    "gulp-concat": "^2.6.0",
    "gulp-htmlmin": "^2.0.0",
    "gulp-if": "^2.0.1",
    "gulp-sass": "^2.3.2",
    "gulp-uglify": "^2.0.0",
    "gulp-useref": "^3.1.0",
    "gulp-postcss": "",
    "gulp-sourcemaps": "",
    "autoprefixer": ""
  },
  "directories": {
    "test": "Sava developer"
  },
  "dependencies": {
    "bower": "^1.8.0",
    "cloud": "^2.0.2",
    "zoom": "0.0.2"
  }
}


### BOWER ###
{
  "name": "New Project",
  "description": "",
  "main": "index.js",
  "authors": [
    "Sava <business.savin@gmail.com>"
  ],
  "license": "ISC",
  "homepage": "",
  "private": true,
  "ignore": [
    "**/.*",
    "node_modules",
    "bower_components"
  ],
  "dependencies": {
    "jquery": "1.8",
    "jquery.maskedinput": "https://github.com/digitalBush/jquery.maskedinput.git",
    "jquery.cookie.js": "https://github.com/carhartl/jquery-cookie.git",
    "cloud-zoom": "https://github.com/smurfy/cloud-zoom.git"
  }
}


### GULP###
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify'); // Минификация JS
var concat = require('gulp-concat'); // Склейка файлов
var htmlmin = require('gulp-htmlmin');// минификация html
var postcss = require('gulp-postcss'); //хаки
var autoprefixer = require('autoprefixer');

//минификация html
gulp.task('minify', function() {
  return gulp.src('*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'))
});

//Конвертируем scss в css
gulp.task('sass', function() {
    return gulp.src('css/style.scss')
     .pipe(sass())
     .pipe(gulp.dest('css/'))
     .pipe(browserSync.reload({
       stream: true
    }))
});

gulp.task('sass_dima', function(){
     return gulp.src('css/style.scss')
     .pipe(sass())
     .pipe(postcss([autoprefixer({
         browsers:['last 5 version']
     })]))
     .pipe(gulp.dest('css/'))
     .pipe(browserSync.reload({
       stream: true
    }))
})

gulp.task('autoprefixer', function () {
    var postcss      = require('gulp-postcss');
    var sourcemaps   = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');

    return gulp.src('css/style.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer({ browsers: ['last 5 versions'] }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('css/'));
});

//Склейка js файлов для продакш
gulp.task('scripts', function() {
//  return gulp.src('js/*.js')
  return gulp.src([
    "js/jquery.min.js",
    "js/jquery.maskedinput.js",
    "js/jquery.cookie.js",
    "js/shopping.cart.1.0.js",
    "js/bootstrap.min.js"
  ])
    .pipe(concat('/js/totall.js'))
    .pipe(uglify())
//    .pipe(uglify('/total/totall.js'))
    .pipe(gulp.dest('js/'));
});


//Создаем виртуальный сервер для прменения изменений сразу
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: '/web/mnogo-buket.ru/HTML/'
    },
  })
})

//Следим за изменениями автоматически
gulp.task('watch', ['browserSync', 'sass_dima', 'autoprefixer'], function (){
    //Обрабатываем сас
    gulp.watch('css/*.scss', ['sass']);
//    gulp.watch(['scripts']);
    // Вешаем автопрефиксы
//    gulp.watch('css/style.css', ['autoprefixer']);
    // Обновляем браузер при любых изменениях в HTML или JS
    gulp.watch('*.html', browserSync.reload);
    //Следим за изменениями в js
//    gulp.watch('js/**/*.js', browserSync.reload);
});

