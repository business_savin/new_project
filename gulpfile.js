var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify'); // Минификация JS
var concat = require('gulp-concat'); // Склейка файлов
var htmlmin = require('gulp-htmlmin');// минификация html
var postcss = require('gulp-postcss'); //хаки
var autoprefixer = require('autoprefixer');

//минификация html
gulp.task('minify', function() {
  return gulp.src('*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'))
});

//Конвертируем scss в css
gulp.task('sass', function() {
    return gulp.src('css/style.scss')
     .pipe(sass())
     .pipe(gulp.dest('css/'))
     .pipe(browserSync.reload({
       stream: true
    }))
});

gulp.task('sass_dima', function(){
     return gulp.src('css/style.scss')
     .pipe(sass())
     .pipe(postcss([autoprefixer({
         browsers:['last 5 version']
     })]))
     .pipe(gulp.dest('css/'))
     .pipe(browserSync.reload({
       stream: true
    }))
})

gulp.task('autoprefixer', function () {
    var postcss      = require('gulp-postcss');
    var sourcemaps   = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');

    return gulp.src('css/style.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer({ browsers: ['last 5 versions'] }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('css/'));
});

//Склейка js файлов для продакш
gulp.task('scripts', function() {
//  return gulp.src('js/*.js')
  return gulp.src([
    "js/jquery.min.js",
    "js/jquery.maskedinput.js",
    "js/jquery.cookie.js",
    "js/shopping.cart.1.0.js",
    "js/bootstrap.min.js"
  ])
    .pipe(concat('/js/totall.js'))
    .pipe(uglify())
//    .pipe(uglify('/total/totall.js'))
    .pipe(gulp.dest('js/'));
});


//Создаем виртуальный сервер для прменения изменений сразу
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: '/web/mnogo-buket.ru/HTML/'
    },
  })
})

//Следим за изменениями автоматически
gulp.task('watch', ['browserSync', 'sass_dima', 'autoprefixer'], function (){
    //Обрабатываем сас
    gulp.watch('css/*.scss', ['sass']);
//    gulp.watch(['scripts']);
    // Вешаем автопрефиксы
//    gulp.watch('css/style.css', ['autoprefixer']);
    // Обновляем браузер при любых изменениях в HTML или JS
    gulp.watch('*.html', browserSync.reload);
    //Следим за изменениями в js
//    gulp.watch('js/**/*.js', browserSync.reload);
});
